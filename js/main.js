const btnBuscar = document.querySelector('.btnBuscar');

llamandoAxios = async (id) => {
    const url = `https://jsonplaceholder.typicode.com/users/${id}`;
    // wheres God
    try {
        console.log(url);
        const respuesta = await axios.get(url);
        const resultado = await respuesta.data;

        if (Object.keys(resultado).length === 0) {
            throw new Error("No hay resultados");
        }
        else if (id <= 0) {
            throw new Error("Número no válido");
        }

        mostrarTodos(resultado);
    }
    catch(error) {
        alert(error.message);
    }
}

const mostrarTodos = (data) => {
    const txtNombre = document.querySelector('.txtNombre');
    const txtNombreUsuario = document.querySelector('.txtNombreUsuario');
    const txtEmail = document.querySelector('.txtEmail');
    const txtCalle = document.querySelector('.txtCalle');
    const txtNumero = document.querySelector('.txtNumero');
    const txtCiudad = document.querySelector('.txtCiudad');

    txtNombre.value = data.name;
    txtNombreUsuario.value = data.username;
    txtEmail.value = data.email;
    txtCalle.value = data.address.street;
    txtNumero.value = data.address.suite;
    txtCiudad.value = data.address.city;
}

btnBuscar.addEventListener('click', () => {
    let id = document.querySelector('.txtId').value;
    llamandoAxios(id);
});
