const btnBuscar = document.querySelector('.btnBuscar');

llamandoFetch = async (name) => {
    const url = `https://restcountries.com/v3.1/name/${name}`;
    // wheres God
    try {
        // console.log(url);
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        // console.log(resultado);
        if (Object.keys(resultado).length === 0) {
            throw new Error("No hay resultados");
        }
        mostrarTodos(resultado);
    }
    catch(error) {
        alert(error.message);
    }
}

const mostrarTodos = (data) => {
    const txtCapital = document.querySelector('.txtCapital');
    const txtLenguaje = document.querySelector('.txtLenguaje');

    txtCapital.value = data[0].capital;
    txtLenguaje.value = Object.values(data[0].languages);
}

btnBuscar.addEventListener('click', () => {
    let pais_str = document.querySelector('.txtPais').value;
    llamandoFetch(pais_str);
});

document.querySelector('.btnLimpiar').addEventListener('click', () => {
    document.querySelector('.txtCapital').value = "";
    document.querySelector('.txtLenguaje').value = "";
    document.querySelector('.txtPais').value = "";
});
