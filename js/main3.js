const btnCargar = document.querySelector('.btnCargar');
const btnVer = document.querySelector('.btnVer');
const breedCb = document.querySelector('#breedCb');

const llamandoFetch = async (url) => {
    // wheres God
    try {
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        if (Object.keys(resultado).length === 0) {
            throw new Error("No hay resultados");
        }
        return resultado;
    }
    catch(error) {
        alert(error.message);
    }
}

const llamandoAxios = async (url) => {
    // wheres God
    try {
        const respuesta = await axios.get(url);
        const resultado = await respuesta.data;
        if (Object.keys(resultado).length === 0) {
            throw new Error("No hay resultados");
        }
        return resultado;
    }
    catch(error) {
        alert(error.message);
    }
}

const limpiarCb = () => {
    breedCb.innerHTML = '';

    const base_element = document.createElement('option');
    base_element.value = 0;
    base_element.innerHTML = "Razas Caninas";
    breedCb.appendChild(base_element);
};

btnCargar.addEventListener('click', async () => {
    const result = await llamandoFetch('https://dog.ceo/api/breeds/list');
    let count = 1;
    limpiarCb();
    for (const op of result.message) {
        const dog = document.createElement('option');
        dog.value = count;
        dog.innerHTML = op;
        breedCb.appendChild(dog);
        ++count;
    }
    btnVer.disabled = false;
});

btnVer.addEventListener('click', async () => {
    if (breedCb.selectedIndex == 0) {
        alert("No válido. Elige una raza, por favor.");
        return;
    }
    
    const current_dog = breedCb.options[breedCb.selectedIndex].innerHTML;
    const current_dog_value = Number(breedCb.value);
    const url = `https://dog.ceo/api/breed/${current_dog}/images/random`;
    let resultados = null;

    if (current_dog_value % 2 == 0)
        resultados = await llamandoFetch(url);
    else
        resultados = await llamandoAxios(url);

    document.querySelector('.dog').src = resultados.message;
});
